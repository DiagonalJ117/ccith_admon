"""
WSGI config for ccith_admon project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os, sys
from django.core.wsgi import get_wsgi_application
sys.path.append('C:/Users/Diagonal/Bitnami Django Stack projects/ccith_admon')
os.environ["DJANGO_SETTINGS_MODULE"] = "ccith_admon.settings"


os.environ["PYTHON_EGG_CACHE"] = "C:/Users/Diagonal/Bitnami Django Stack projects/ccith_admon/egg_cache"







application = get_wsgi_application()

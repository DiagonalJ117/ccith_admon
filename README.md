# Registro de Acceso a Centro de Cómputo

Plataforma para el registro de acceso de usuarios al Centro de Cómputo

* Registro de usuario, razón para usar el centro y hora de entrada al mismo.

* Registro de incidencias.

* Bloqueo a usuarios con mayor a `n` incidencias

## Backend

Backend hecho en Django 2.2

<img src="https://static.djangoproject.com/img/logos/django-logo-positive.png" align="center">

## Frontend

Frontend hecho con Bootstrap 4

<img src="https://cdn.worldvectorlogo.com/logos/bootstrap-4.svg" align="center">
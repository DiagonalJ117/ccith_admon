import os
from datetime import datetime, timedelta
#from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin 
#from django.contrib.auth.base_user import BaseUserManager
from django.db import models

class Solicitante(models.Model):
    matricula = models.CharField(max_length=10, blank=False, primary_key=True, unique=True)
    nombre = models.CharField(max_length=20, blank=False)
    ap_pat = models.CharField(max_length=20, blank=True, null=True)
    ap_mat = models.CharField(max_length=20, blank=True, null=True)
    carrera = models.CharField(max_length=30, blank=True, null=True)
    #bloqueado = models.BooleanField(default=False)

    def __str__(self):
        return self.matricula

class Registro(models.Model):
    RAZON_USO_CHOICES=(
        ('TAREA', 'Tarea'),
        ('ESTUDIO', 'Estudio'),
        ('INVESTIGACION', 'Investigación'),
    )
    
    solicitante = models.ForeignKey(Solicitante, default=None, on_delete=models.CASCADE)
    fecha_hora = models.DateTimeField(blank=True, null=True)
    razon_uso = models.CharField(max_length=20, choices=RAZON_USO_CHOICES, default="TAREA")
   

    def __str__(self):
        return "Prestamo " + str(self.id)



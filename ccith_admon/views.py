import datetime
import csv
from datetime import date, timedelta
from django.db.models import Count
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from .models import Registro, Solicitante
from .forms import RegistroCC_Form, RegistroSolicitante_Form
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required, permission_required

@login_required(login_url="/ccith_admon/accounts/login/")
def dashboard(request):
    today = timezone.now()
    registros = Registro.objects.all()

    return render(request, 'dashboard.html', {'registros':registros})


def solicitud(request):

   
    if request.method == 'POST':
        try:
            solicitante = Solicitante.objects.get(matricula=request.POST.get('solicitante'))
        except Solicitante.DoesNotExist:
            messages.error(request, mark_safe('No. de Control '+str(request.POST.get('solicitante')) + ' no existente en el sistema. <strong>Verifique que lo haya ingresado correctamente</strong> o <a href="/ccith_admon/registrar_solicitante/">Registrese aquí</a>'))
            return redirect('solicitud')
        form = RegistroCC_Form(request.POST)
        if form.is_valid():
            solicitud = form.save(commit=False)
            
            solicitud.fecha_hora=timezone.now()
            solicitud.save()
            messages.success(request, 'Aprobada')
            return redirect('solicitud')
    else:
        form = RegistroCC_Form()
    
    return render(request, 'solicitud.html', { 'form': form})

def registrar_solicitante(request):
    if request.method == 'POST':
        form = RegistroSolicitante_Form(request.POST)

        if form.is_valid():
            messages.success(request, 'Solicitante Registrado Correctamente')
            instance = form.save(commit=False)
            instance.save()
            return redirect('solicitud')
        else:
            messages.error(request, 'Error al Registrar')
    else:
        form = RegistroSolicitante_Form()

    return render(request, 'add_solicitante.html', {'form': form})

def exportar_csv_registros(request):

    output = []
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename="CC_registros_data.csv"'

    writer = csv.writer(response)
    query_set = Registro.objects.all()
    writer.writerow(['id', 'Solicitante', 'fecha_hora', 'razon_uso'])
    for registro in query_set:
        output.append([registro.id, registro.solicitante, registro.fecha_hora, registro.razon_uso])
    writer.writerows(output)
    return response

def exportar_csv_solicitantes(request):

    output = []
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename="CC_solicitantes_data.csv"'

    writer = csv.writer(response)
    query_set = Solicitante.objects.all()
    writer.writerow(['No. Control', 'Nombre', 'Apellido_Paterno', 'Apellido_Materno', 'Carrera'])
    for solicitante in query_set:
        output.append([solicitante.matricula, solicitante.nombre, solicitante.ap_pat, solicitante.ap_mat, solicitante.carrera])
    writer.writerows(output)
    return response










    
        


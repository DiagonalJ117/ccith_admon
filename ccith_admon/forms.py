from django.db import models
from django import forms
from .models import Registro, Solicitante
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

class RegistroCC_Form(forms.ModelForm):
    required_css_class = 'required'
    def clean_solicitante(self):
        solicitante = self.cleaned_data.get('solicitante')
        try:
            Solicitante.objects.get(matricula=solicitante)
        except Solicitante.DoesNotExist:
            msg = forms.ValidationError('Matricula no existente en el sistema.')
            self.add_error('solicitante', msg)
            raise msg
        return solicitante

    class Meta:
        model = Registro
        fields = ['solicitante', 'razon_uso']
        labels = {
            'razon_uso': 'Razón de uso'
        }

class RegistroSolicitante_Form(forms.ModelForm):
    required_css_class = 'required'
    def clean_matricula(self):
        matricula = self.cleaned_data.get('matricula').upper()
        r = Solicitante.objects.filter(matricula=matricula)
        if r.count():
            raise ValidationError("Matricula ya existente")
        return matricula

    def clean_carrera(self):
        carrera = self.cleaned_data.get('carrera').upper()
    class Meta:
        model = Solicitante
        fields = ['matricula', 'nombre', 'ap_pat', 'ap_mat', 'carrera']
        labels = {
            'matricula' : 'No. Control',
            'ap_pat' : 'Apellido Paterno',
            'ap_mat' : 'Apellido Materno'
        }




from django.contrib import admin
from .models import Registro, Solicitante

# Register your models here.
admin.site.register(Solicitante)
admin.site.register(Registro)

admin.site.site_header = "Sala de Computo"
admin.site.site_title = "Panel de Admin"
"""prestaproyector URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from ccith_admon import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.solicitud, name="solicitud"),
    path('registrar_solicitante/', views.registrar_solicitante, name="add_solicitante"),
    path('accounts/', include('accounts.urls')),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('exportar_csvs_registros/', views.exportar_csv_registros, name="exportar_csv_registros"),
    path('exportar_csvs_solicitantes/', views.exportar_csv_solicitantes, name="exportar_csv_solicitantes"),
    #path('devolucion/<id>', views.devolucion, name="devolucion"),
    #path('marcar_incidencia/<id>', views.marcar_incidencia, name="marcar_incidencia"),
    #path('proyectores/', views.status_proyectores, name="proyectores"),
    #path('proyector_status_update/<id>', views.proyector_status_update, name="proyector_status_update"),
    #path('solicitantes/', views.solicitantes, name="solicitantes"),
    #path('bloquear_solicitante/<id>', views.bloquear_solicitante, name="bloquear_solicitante")
]
